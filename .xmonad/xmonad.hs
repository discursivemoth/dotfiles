import Data.List (sortBy)
import Data.Function (on)
import Control.Monad (forM_, join)
import qualified XMonad.StackSet as W

import XMonad
import XMonad.Config.Desktop
import XMonad.Util.NamedWindows
import XMonad.Util.Run
import XMonad.Actions.DynamicWorkspaces
main = do
  forM_ [".xmonad-workspace-log", ".xmonad-title-log"] $ \file -> do
    safeSpawn "mkfifo" ["/tmp/" ++ file]
    
  xmonad $ defaultConfig
    {
     -- simple stuff
     terminal           = "xterm"
    ,logHook         = eventLogHook
    ,modMask            = mod4Mask
    , workspaces = myWorkspaces
    }

wsAV    = "AV"
wsBSA   = "BSA"
wsCOM   = "COM"
wsDOM   = "DOM"
wsDMO   = "DMO"
wsFLOAT = "FLT"
wsGEN   = "GEN"
wsGCC   = "GCC"
wsMON   = "MON"
wsOSS   = "OSS"
wsRAD   = "RAD"
wsRW    = "RW"
wsSYS   = "SYS"
wsTMP   = "TMP"
wsVIX   = "VIX"
wsWRK   = "WRK"
wsWRK2  = "WRK:2"
wsGGC   = "GGC"

-- myWorkspaces = map show [1..9]
myWorkspaces = [wsGEN, wsWRK, wsWRK2, wsSYS, wsMON, wsFLOAT, wsRW, wsTMP]

eventLogHook = do
  winset <- gets windowset
  title <- maybe (return "") (fmap show . getName) . W.peek $ winset
  let currWs = W.currentTag winset
  let wss = map W.tag $ W.workspaces winset
  let wsStr = join $ map (fmt currWs) $ sort' wss

  io $ appendFile "/tmp/.xmonad-title-log" (title ++ "\n")
  io $ appendFile "/tmp/.xmonad-workspace-log" (wsStr ++ "\n")

  where fmt currWs ws
          | currWs == ws = "[" ++ ws ++ "]"
          | otherwise    = " " ++ ws ++ " "
        sort' = sortBy (compare `on` (!! 0))